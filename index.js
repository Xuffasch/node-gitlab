const PORT = 3000;

const express = require('express');
const app = express();

app.use(express.json());
app.use(express.urlencoded({extended: false}));

var logger = require('morgan');
app.use(logger('dev'));

app.route("/")
   .get((req, res) => {
     if (Date() > new Date(2020, 01, 01)) {
        res.send(`Happy New Year 2020 !`)
     } else 
        res.send(`It's almost 2020 ! at ${Date()}`)
    });

app.listen(
  { port: PORT },
  () => console.log(`git-practice node server start at ${Date()}`)
)