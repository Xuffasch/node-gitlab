const client = [{ id: 1, lastName: "Thim"},
                { id: 2, lastName: "Hill"},
                { id: 3, lastName: "May"}]

exports.client = client;

exports.allClients = function() {
  return client;
}

exports.firstClient = function() {
  return client[0];
}

exports.lastClient = function() {
  return client[client.length-1];
}

exports.secondClient = function() {
  return client[1];
}