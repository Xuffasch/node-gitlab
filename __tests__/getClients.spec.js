const { allClients, firstClient, client } = require("../dataSource/clientSource");

describe("Get all clients", () => {
  test("it should get the list of all clients", () => {
    const output = client;

    expect(allClients()).toEqual(output);
  })
});

describe("Get first client", () => {
  test("it should get the first client", () => {
    const output = client[0];

    expect(firstClient()).toEqual(output);
  })
});