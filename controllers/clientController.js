const { allClients, firstClient } = require("../dataSource/clientSource");

exports.getClients = function(req, res) {
  console.log("GET all clients");
  res.status(200)
     .send({ result: allClients() });
}

exports.getFirst = function(req, res) {
  console.log("GET first client");
  res.status(200)
     .send({ result: firstClient() });
}